@extends('layouts.main')

@section('content')

    {{ $agencies->title }}<br>
    {{ $agencies->about }}

    {!! form($form) !!}

@endsection