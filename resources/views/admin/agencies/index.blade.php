@extends('layouts.main')


@section('content')

    <hr>
    <h3 style="text-align: right">لیست آژانس ها</h3>
    <hr>

    @foreach($agencies as $agencie)


        <div class="card border-primary mb-3" style="text-align: right">
            <div class="card-header">
                <a href="{{  route('agencies.show',['id'=>$agencie->id])    }}">
                    <span style="text-align: left; float: right">{{$agencie->title}}</span>
                </a>

                <a href="{{  route('tour.create',['id'=>$agencie->id]) }}" style="float: left">افزودن تور</a>

            </div>
            <div class="card-body">
                <p class="card-text">{{$agencie->title}}</p>
            </div>
        </div>


    @endforeach


@endsection