@extends('layouts.main')

@section('content')

    <hr>

    @foreach($articles as $article)

        <ul>
            <h2><a href="{{route('articles.show',$article->id)}}">{{$article->title}}</a></h2><br>

            <div class="body">
                <h6>{{$article->body}}</h6>
            </div>
        </ul>
        <hr>



        <ul>

        </ul>

    @endforeach
@endsection