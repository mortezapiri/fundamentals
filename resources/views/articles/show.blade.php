@extends('layouts.main')

@section('content')
    <h1>{{$article->title}}</h1>

    {{$article->body}}

@endsection