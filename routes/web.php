<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('test', 'TestController@index');



//Route::get('articles', 'ArticlesController@index');
//Route::get('articles/create', 'ArticlesController@create');
//Route::post('articles/create', 'ArticlesController@store')->name('articles.store');
//Route::get('articles/{id}', 'ArticlesController@show')->name('articles.show');
//Route::get('articles/{id}/edit', 'ArticlesController@edit')->name('articles.edit');


Route::resource('articles','ArticlesController');

Route::get('foo',['middleware'=>'manager',function(){
    return 'managers can see this page';
}]);

Route::group(['prefix' => 'admin'], function () {
    Route::resource('agencies', 'AgenciesController');
});

Route::group(['prefix' => 'admin'], function () {
    Route::resource('tours', 'TourController');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
