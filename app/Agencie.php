<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agencie extends Model
{
    protected $guarded=['id'];

    public function tour()
    {
        return $this->hasMany(Agencie::class);
    }
}
