<?php

namespace App\Form;

use Kris\LaravelFormBuilder\Form;

class AgencieForm extends Form
{
    /**
     * @return mixed|void
     */
    public function buildForm()
    {
        $this->add('title','text')
            ->add('about','textarea')
            ->add('submit','submit',[
                'label' => 'ایجاد آژانس',
                'attr'=>[
                    'btn'  => 'btn-success',
                ]
            ]);
    }
}
