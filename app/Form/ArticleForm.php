<?php

namespace App\Form;

use Kris\LaravelFormBuilder\Form;

class ArticleForm extends Form
{
    public function buildForm()
    {
        $this->add('title','text',[
            'rules' =>'required'
        ])
            ->add('body','textarea',[
                'rules' =>'required'
            ])
            ->add('submit','submit',[
                'label'=>'add to articles'
            ]);
    }
}
