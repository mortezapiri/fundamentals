<?php

namespace App\Form;

use Kris\LaravelFormBuilder\Form;

use App\Tour;

class TourForm extends Form
{

    public function buildForm()
    {
        $this->add('title', 'text')
            ->add('content', 'textarea')
            ->add('submit', 'submit', [
                'label' => 'ایجاد تور',
            ]);
    }
}