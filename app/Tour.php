<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{

    protected $fillable = ['title','content','agencies_id'];

    protected $guarded = ['id'];

    public function agencies()
    {
        return $this->belongsTo(Agencie::class);
    }
}
