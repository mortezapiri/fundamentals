<?php

namespace App\Http\Controllers;

use App\Article;
use DeepCopy\f002\A;
use Illuminate\Http\Request;

use App\Form\ArticleForm;

class ArticlesController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth', ['only' => 'create']);
    }

    public function index()
    {
        $articles = Article::latest()->get();

        return view('articles.index', compact('articles'));
    }

    public function show($id)
    {
        $article = Article::findOrFail($id);
        return view('articles.show', compact('article'));
    }

    public function create()
    {

        if (\Auth::guest()) {
            return redirect('articles');
        }

        $form = \FormBuilder::create(ArticleForm::class, [
            'method' => 'POST',
            'url' => route('articles.store')
        ]);


        return view('articles.create', compact('form'));

    }


    public function store(Request $request)
    {


        $articles = new Article($request->all());

        \Auth::user()->articles()->save($articles);

        return redirect()->route('articles.show', ['id' => $articles->id]);
    }

    public function edit($id)
    {
        $articles = Article::findOrFail($id);

        $form = \FormBuilder::create(ArticleForm::class, [
            'method' => 'PUT',
            'url' => route('articles.update', ['id' => $articles->id]),
            'model' => $articles,
        ]);

        return view('articles.edit', compact('form'));
    }


    public function update(Request $request, $id)
    {
        $articles = Article::findOrFail($id);
        $articles->update($request->only('title', 'body'));
        return redirect()->route('articles.edit', ['id' => $articles->id]);
    }


}
