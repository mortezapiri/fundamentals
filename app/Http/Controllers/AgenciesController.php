<?php

namespace App\Http\Controllers\admin;


namespace App\Http\Controllers;

use App\Form\AgencieForm;
use Illuminate\Http\Request;

use App\form\TourForm;

use App\Tour;

use App\Agencie;

class AgenciesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $agencies = Agencie::all();

        return view('admin.agencies.index', compact('agencies'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $form = \FormBuilder::create(AgencieForm::class, [
            'method' => 'POST',
            'url' => route('agencies.store')
        ]);

        return view('admin.agencies.create', compact('form'));

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $agencie = Agencie::create($request->only('title', 'about'));

        return redirect()->route('agencies.edit', ['id' => $agencie->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $agencies = Agencie::findOrFail($id);

        $form = \FormBuilder::create(TourForm::class, [
            'method' => 'POST',
            'url' => route('tours.store')
        ]);

        return view('admin.agencies.show', compact('agencies', 'form'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $agencies = Agencie::findOrFail($id);

        $form = \FormBuilder::create(AgencieForm::class, [
            'method' => 'PUT',
            'url' => route('agencies.update', ['id' => $agencies->id]),
            'model' => $agencies,
        ]);

        return view('admin.agencies.create', compact('form'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $agencies = Agencie::findOrFail($id);

        $agencies->update($request->only('title', 'about'));

        return redirect()->route('agencies.edit', ['id' => $agencies->id]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
