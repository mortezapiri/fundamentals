<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    // user can have many articles

    // an article owned by a user

    public function articles()
    {
        return $this->hasMany('App\Article');
    }

    public function isATeamManager()
    {
        return false;
    }


    public function tags()
    {
        $this->belongsToMany(Tag::class)->withTimestamps();
    }


}
